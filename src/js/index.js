AOS.init();

function ShowMenu(idClick, idOpen, idClose, idwindow) {
  if (idClick && idOpen && idClose) {
    idClick.addEventListener("click", function () {
      idOpen.classList.add("active");
    });
    idClose.addEventListener("click", function () {
      idOpen.classList.remove("active");
    });
    window.addEventListener("click", function (event) {
      let window = event.composedPath();
      let CheckClick = window.includes(idClick);
      let CheckOpen = window.includes(idwindow);
      if (!CheckClick && !CheckOpen) {
        idOpen.classList.remove("active");
      }
    });
  }
}

let Arrclick = Array.prototype.slice.call(
  document.querySelectorAll(".ActClick")
);

let Arropen = Array.prototype.slice.call(
  document.querySelectorAll(".ArrayOpen")
);

let ArrClose = Array.prototype.slice.call(
  document.querySelectorAll(".ActClose")
);

let ArrWindow = Array.prototype.slice.call(
  document.querySelectorAll(".Window")
);

if (Arrclick.length && Arropen.length && ArrClose.length) {
  Arrclick.forEach(function (item, index) {
    ShowMenu(item, Arropen[index], ArrClose[index], ArrWindow[index]);
  });
}

$(".jewelry__right").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: ".jewelry__left",
});
$(".jewelry__left").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: ".jewelry__right",
  dots: true,
  centerMode: true,
  focusOnSelect: true,
});

$(".shop__right").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 2000,
  dots: true,
  responsive: [
    {
      breakpoint: 781,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});

$(".earrings__content").slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 781,
      settings: {
        slidesToShow: 3,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});

$(".thumbnailgeneral").slick({
  dots: true,
  infinite: true,
  speed: 500,
  fade: true,
  cssEase: "linear",
});

let LocalStorageKey = "myCat";

let amount = Array.prototype.slice.call(
  document.querySelectorAll(".site-overlay")
);

function GetLocalStorage(key) {
  let StorageString = localStorage.getItem(key);
  let CheckStorage = StorageString ? JSON.parse(StorageString) : {};
  return CheckStorage;
}

function SavelocalStorage(key, value) {
  let StorageString = localStorage.getItem(key);
  let CheckStorage = StorageString ? JSON.parse(StorageString) : {};
  let totalall = Object.keys(CheckStorage).length;
  if (totalall) {
    CheckStorage = { ...CheckStorage, [totalall]: value };
  } else {
    CheckStorage = { 0: value };
  }
  localStorage.setItem(LocalStorageKey, JSON.stringify(CheckStorage));
}

function Sumamount() {
  let CartItem = GetLocalStorage(LocalStorageKey);
  if (Object.values(CartItem).length) {
    let CartTotal = Object.values(CartItem).map((cart) => {
      return cart.amount;
    });
    let sumplus = CartTotal.reduce(function (a, b) {
      return a + b;
    });
    let positioncart = document.querySelector(".header__cartPC .PC span");
    positioncart.innerHTML = sumplus;
  }
}

console.log(amount);

amount.forEach(function (item) {
  let less = item.querySelector(".less");
  let plus = item.querySelector(".plus");
  let input = item.querySelector("input");
  let price = item.querySelector(".price");
  let total = parseInt(price.innerHTML.replace("$", ""));

  less.addEventListener("click", function () {
    input.value > 1 && input.value--;
    price.innerHTML = "$" + total * input.value + ".00";
  });

  plus.addEventListener("click", function () {
    input.value++;
    price.innerHTML = "$" + total * input.value + ".00";
  });

  let addToCart = item.querySelector(".add");
  addToCart.addEventListener("click", function () {
    let name = item.querySelector(".textgeneral h2").innerText;
    let amount = parseFloat(input.value);
    let money = item.querySelector(".price").innerText;

    let product = { name, amount, money };
    SavelocalStorage(LocalStorageKey, product);
    Sumamount();
  });
});

let buttonCart = document.querySelector(".header__cartPC .PC");

buttonCart.addEventListener("click", function () {});

// --------------------

$(".feedback__note--text").slick({
  dots: true,
});

// Scroll Header

let scrollHeader = document.querySelector(".header");
let HeightBanner = document.querySelector(".header").offsetHeight;

var lastScrollTop = HeightBanner;

// element should be replaced with the actual target element on which you have applied scroll, use window in case of no target element.
window.addEventListener(
  "scroll",
  function () {
    // or window.addEventListener("scroll"....
    var st = window.pageYOffset || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"
    if (st > lastScrollTop) {
      scrollHeader.classList.add("active");
    } else {
      scrollHeader.classList.remove("active");
    }
    lastScrollTop = st <= HeightBanner ? HeightBanner : st; // For Mobile or negative scrolling
    //    if(st <=0){
    //     lastScrollTop =0
    //    }else{
    //     lastScrollTop = st
    //    }
  },
  false
);
